FROM ubuntu:20.04

WORKDIR /root/.node-red/

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Europe/Paris

RUN apt -y update
RUN apt -y install wget
RUN apt -y install sudo

RUN apt -y install curl
RUN apt -y install git
RUN apt -y install build-essential
RUN apt -y install bash

RUN wget https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered
RUN chmod +x ./update-nodejs-and-nodered 
RUN yes | ./update-nodejs-and-nodered

RUN apt-get -y install default-jdk

RUN cd ~/.node-red

RUN wget https://repo1.maven.org/maven2/com/nuodb/jdbc/nuodb-jdbc/23.0.0/nuodb-jdbc-23.0.0.jar

EXPOSE 80

CMD ["node-red"]